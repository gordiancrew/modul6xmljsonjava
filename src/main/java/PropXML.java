import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PropXML {

    private Map<String, String> propList = new HashMap<>();
    final static String RESOURCE_FILE = "src/main/resources/";

    public PropXML() {

    }

    public void parseToObject(String PATH) throws ParserConfigurationException, IOException, SAXException {

        File prop = new File(PATH);
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        builder = factory.newDocumentBuilder();
        Document document = builder.parse(prop);
        NodeList r = document.getElementsByTagName("Property");
        for (int i = 0; i < r.getLength(); i++) {
            NodeList r2 = r.item(i).getChildNodes();
            for (int t = 0; t < r2.getLength(); t++) {
                if (r2.item(t).getNodeName() == "Key" && r2.item(t + 2).getNodeName() == "Value") {
                    propList.put(r2.item(t).getTextContent(), r2.item(t + 2).getTextContent());
                    // r2.item(t).setTextContent("YES!");

                }
            }
        }
    }

    public void writeXMLFile(String fileName) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document doc = dbf.newDocumentBuilder().newDocument();
        Element properties = doc.createElement("Properties");
        doc.appendChild(properties);
        File file = new File(fileName);
        for (String key : propList.keySet()) {
            Element property = doc.createElement("Property");
            properties.appendChild(property);
            Element keyProp = doc.createElement("Key");
            keyProp.setTextContent(key);
            property.appendChild(keyProp);
            Element value = doc.createElement("Value");
            value.setTextContent(propList.get(key));
            property.appendChild(value);
        }
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(new DOMSource(doc), new StreamResult(file));
    }

    public void setProp(String key, String value) {
        propList.put(key, value);
    }

    public void removeProp(String key) {
        propList.remove(key);
    }

    public Map<String, String> getPropList() {
        return propList;
    }

    public void setPropList(Map<String, String> propList) {
        this.propList = propList;
    }


}
