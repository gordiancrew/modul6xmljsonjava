import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

import java.util.HashMap;
import java.util.Map;


public class CreateFileXML {
    Document doc;
    Element root;
    Element properties;
    PropXML prop1;
    private long timer;
    static final String XML_FILES = "src/main/resources/ResultFilesXML/" + App.TIME_STAMP + ".xml";

    public CreateFileXML() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        doc = dbf.newDocumentBuilder().newDocument();
        root = doc.createElement("Task");
        doc.appendChild(root);
        properties = doc.createElement("Properties");
        root.appendChild(properties);
    }

    public void createFileXMLPropFiles(String prName, HashMap<String, String> oldNewNames) {


        Element propertiesFilesNames = doc.createElement("Property");


        Element path = doc.createElement("Path");
        propertiesFilesNames.appendChild(path);
        Element configs = doc.createElement("Configs");
        propertiesFilesNames.appendChild(configs);
        String value = prop1.getPropList().get(prName);
        path.setTextContent(prName + "=" + value);
        properties.appendChild(propertiesFilesNames);
        for (String oldName : oldNewNames.keySet()) {

            Element config = doc.createElement("Config");
            configs.appendChild(config);
            Element oldNames = doc.createElement("OldName");
            oldNames.setTextContent(oldName);
            config.appendChild(oldNames);
            Element newName = doc.createElement("NewName");
            newName.setTextContent(oldNewNames.get(oldName));
            config.appendChild(newName);
        }
    }


    public void printXML2() throws TransformerException {

        File file = new File(XML_FILES);
        root.setAttribute("executionTime", Long.toString(timer));
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(new DOMSource(doc), new StreamResult(file));
    }

    public void setProp1(PropXML prop1) {
        this.prop1 = prop1;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }
}
