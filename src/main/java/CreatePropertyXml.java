import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

public class CreatePropertyXml {
    public static void main(String[] args) throws TransformerException, ParserConfigurationException, IOException, SAXException {
        PropXML proper1xml = new PropXML();
        proper1xml.setProp("ID_ONE", "src/main/resources/proper2.xml");
        proper1xml.setProp("ID_TWO", "src/main/resources/proper3.xml");
        proper1xml.writeXMLFile("proper1");

        PropXML proper2xml = new PropXML();
        proper2xml.setProp("ID_THREE", "3");
        proper2xml.setProp("ID_FOUR", "4");
        proper2xml.writeXMLFile("src/main/resources/proper2.xml");

        PropXML proper3xml = new PropXML();
        proper3xml.setProp("ID_FIVE", "5");
        proper3xml.setProp("ID_SIX", "6");
        proper3xml.writeXMLFile("src/main/resources/proper3.xml");
    }
}
