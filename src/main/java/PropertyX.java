import java.util.HashMap;

class PropertiesX {

    private String propertyX;
    private String valueX;
    private HashMap<String, String> oldNewX = new HashMap<>();

    public String getPropertyX() {
        return propertyX;
    }

    public void setPropertyX(String propertyX) {
        this.propertyX = propertyX;
    }

    public String getValueX() {
        return valueX;
    }

    public void setValueX(String valueX) {
        this.valueX = valueX;
    }

    public HashMap<String, String> getOldNewX() {
        return oldNewX;
    }

    public void putOldNew(String oldName, String newName) {
        oldNewX.put(oldName, newName);
    }

    public void setOldNewX(HashMap<String, String> oldNewX) {
        this.oldNewX = oldNewX;
    }
}
