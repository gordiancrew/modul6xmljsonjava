//import com.sun.org.slf4j.internal.LoggerFactory;


import org.xml.sax.SAXException;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.logging.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;


public class App {
    static Logger logger = java.util.logging.Logger.getLogger(App.class.getName());
    final static String PROPERTIES = "src/main/resources/proper1.xml";
    final static String FILE_HANDLER = "src/main/resources/logs/";
    final static String TIME_STAMP = new SimpleDateFormat("MM_dd__HH_mm_ss").format(new Date());

    public static void main(String[] args) throws IOException, TransformerException, ParserConfigurationException, SAXException {
        long startTime = System.currentTimeMillis();
        FileHandler fh = new FileHandler(FILE_HANDLER + TIME_STAMP);
        fh.setFormatter(new MyFormatter());
        logger.addHandler(fh);
        logger.info("Start project");
        CreateFileXML cfx = new CreateFileXML();
        PropXML prop1 = new PropXML();
        prop1.parseToObject(PROPERTIES);

        if (fileExist(prop1)) {
            logger.info("All files is exist!!");
            cfx.setProp1(prop1);
            for (String key : prop1.getPropList().keySet()) {
                fileConfigRename(prop1.getPropList().get(key), key, cfx);
            }
        } else {
            logger.info("All file is not exist!!!");
        }
        logger.info("Finish project!");
        fh.close();
        CreateFileJSON.PrintFileJSON();
        long stopTime = System.currentTimeMillis();
        cfx.setTimer(stopTime - startTime);
        cfx.printXML2();
    }

    public static void fileConfigRename(String path, String propName, CreateFileXML cfx) throws IOException, ParserConfigurationException, SAXException, TransformerException {

        PropXML config = new PropXML();
        config.parseToObject(path);
        HashMap<String, String> oldNewName = new HashMap<>();
        ArrayList<String> list = new ArrayList<>(config.getPropList().keySet());
        for (String configName : list) {
            String newName = newName(configName);
            String value = config.getPropList().get(configName);
            oldNewName.put(configName, newName);
            if (!newName.equals(configName)) {
                config.removeProp(configName);
                config.setProp(newName, value);
                config.writeXMLFile(path);
            }
        }
        cfx.createFileXMLPropFiles(propName, oldNewName);
    }

    public static boolean fileExist(PropXML prop1) throws IOException {

        boolean exist = true;
        for (String key : prop1.getPropList().keySet()) {
            File file = new File(prop1.getPropList().get(key));
            if (!(file.exists())) {
                exist = false;
            }
        }
        return exist;
    }

    public static String newName(String name) {
        String[] list = name.split("_");
        StringBuilder sb = new StringBuilder();
        sb.append("NEW_NAME_");
        sb.append(list[list.length - 1]);
        logger.info("File " + name + " rename to " + sb.toString());
        return sb.toString();
    }

    static class MyFormatter extends XMLFormatter {
        @Override
        public String format(LogRecord record) {
            StringBuilder sb = new StringBuilder(500);
            sb.append("<record>\n");
            final Instant instant = record.getInstant();
            sb.append("  <date>");
            sb.append(instant);
            sb.append("</date>\n");
            sb.append("  <level>");
            sb.append(record.getLevel());
            sb.append("</level>\n");
            sb.append("  <message>");
            sb.append(record.getMessage());
            sb.append("</message>");
            sb.append("\n");
            sb.append("</record>\n");
            return sb.toString();
        }
    }
}